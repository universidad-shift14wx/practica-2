module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires AnimateFX;
    opens org.example to javafx.fxml;
    exports org.example;
}