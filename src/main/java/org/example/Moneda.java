package org.example;

public class Moneda {

     final String simbolo;
    final Double tasa; // o valor

    Moneda(String simbolo, Double tasa) {
        this.simbolo = simbolo;
        this.tasa = tasa;
    }
}
