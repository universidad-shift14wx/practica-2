package org.example;

import animatefx.animation.FadeIn;
import animatefx.animation.FadeInUp;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.*;

public class PrimaryController implements Initializable {
    @FXML
    Button one;
    @FXML
    Button two;

    public PrimaryController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        one.setVisible(false);
        two.setVisible(false);
        //probando como funcionan threads
        Thread mk = new Thread(new makeFancyThingsHappens(one));
        Thread mk2 = new Thread(new makeFancyThingsHappens(two));
        mk.start();
        mk2.start();
    }

    @FXML
    void GotoOne() throws IOException {
        App.setRoot("e1");
    }

    @FXML
    void GotoSecond() throws IOException {
        App.setRoot("e2");
    }
}


class makeFancyThingsHappens implements Runnable{

    Button futureButtonShown;

    makeFancyThingsHappens(Button btn){
        futureButtonShown = btn;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1500);
            futureButtonShown.setVisible(true);
            new FadeIn(futureButtonShown).play();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
