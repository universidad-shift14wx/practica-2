# JAVAFX con IntelliJ

Practica #2 para POE.


# Screen
![working app](https://i.imgur.com/g1dQs9L.gif)

# Stack
**JDK** : 11
**IDE** : IntelliJ 2019.3

# Libs
**BootstrapFX** : [https://github.com/kordamp/bootstrapfx](https://github.com/kordamp/bootstrapfx)
**# AnimateFX** : [https://github.com/Typhon0/AnimateFX](https://github.com/Typhon0/AnimateFX)
# info
Aplicacion modular hecha desde 0 siguiendo el tutorial oficial de **openFx**: [https://openjfx.io/openjfx-docs/](https://openjfx.io/openjfx-docs/)